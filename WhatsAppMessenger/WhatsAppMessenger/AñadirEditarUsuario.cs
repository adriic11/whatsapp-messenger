﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhatsAppMessenger
{
    public partial class AñadirEditarUsuario : Form
    {
        object obj;
        public AñadirEditarUsuario(object obj)
        {
            InitializeComponent();
            this.obj = obj;
        }

        private void AñadirEditarUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                if (obj == null)
                {
                    if (General.BD.Usuario.FindByIdUsuario(txtTelefono.Text) == null)
                    {
                        AppData.UsuarioRow row = General.BD.Usuario.NewUsuarioRow();
                        row.IdCuenta = Properties.Settings.Default.Telefono;
                        row.IdUsuario = txtTelefono.Text;
                        row.Nombre = txtNombre.Text;
                        General.BD.Usuario.AddUsuarioRow(row);
                        General.BD.Usuario.AcceptChanges();
                        General.BD.Usuario.WriteXml(string.Format("{0}\\usuario.dat", Application.StartupPath));
                        e.Cancel = false;
                    }
                    else
                    {
                        MessageBox.Show("Este número de teléfono ya existe.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                    }
                }
                else
                {
                    AppData.UsuarioRow row = General.BD.Usuario.FindByIdUsuario(obj.GetType().GetProperty("Telefono").GetValue(obj, null).ToString());
                    if (row != null)
                    {
                        row.IdUsuario = txtTelefono.Text;
                        row.Nombre = txtNombre.Text;
                        General.BD.AcceptChanges();
                        General.BD.Usuario.WriteXml(string.Format("{0}\\usuario.dat", Application.StartupPath));
                    }
                }
            }
        }

        private void AñadirEditarUsuario_Load(object sender, EventArgs e)
        {
            if (obj != null)
            {
                txtTelefono.Text = obj.GetType().GetProperty("Telefono").GetValue(obj, null).ToString();
                txtNombre.Text = obj.GetType().GetProperty("Nombre").GetValue(obj, null).ToString();
            }
        }
    }
}
