﻿namespace WhatsAppMessenger
{
    partial class Registro
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpRequestCode = new System.Windows.Forms.GroupBox();
            this.btnSolicitud = new System.Windows.Forms.Button();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.grpConfirmar = new System.Windows.Forms.GroupBox();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSmsCode = new System.Windows.Forms.TextBox();
            this.grpRequestCode.SuspendLayout();
            this.grpConfirmar.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRequestCode
            // 
            this.grpRequestCode.Controls.Add(this.btnSolicitud);
            this.grpRequestCode.Controls.Add(this.txtNombre);
            this.grpRequestCode.Controls.Add(this.label2);
            this.grpRequestCode.Controls.Add(this.label1);
            this.grpRequestCode.Controls.Add(this.txtTelefono);
            this.grpRequestCode.Location = new System.Drawing.Point(12, 24);
            this.grpRequestCode.Name = "grpRequestCode";
            this.grpRequestCode.Size = new System.Drawing.Size(331, 127);
            this.grpRequestCode.TabIndex = 1;
            this.grpRequestCode.TabStop = false;
            this.grpRequestCode.Text = "Solicitar código";
            // 
            // btnSolicitud
            // 
            this.btnSolicitud.Location = new System.Drawing.Point(250, 95);
            this.btnSolicitud.Name = "btnSolicitud";
            this.btnSolicitud.Size = new System.Drawing.Size(75, 23);
            this.btnSolicitud.TabIndex = 3;
            this.btnSolicitud.Text = "Enviar";
            this.btnSolicitud.UseVisualStyleBackColor = true;
            this.btnSolicitud.Click += new System.EventHandler(this.btnSolicitud_Click);
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(95, 31);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(230, 20);
            this.txtTelefono.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Telefono:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(95, 69);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(230, 20);
            this.txtNombre.TabIndex = 2;
            // 
            // grpConfirmar
            // 
            this.grpConfirmar.Controls.Add(this.btnConfirmar);
            this.grpConfirmar.Controls.Add(this.label4);
            this.grpConfirmar.Controls.Add(this.txtSmsCode);
            this.grpConfirmar.Location = new System.Drawing.Point(12, 173);
            this.grpConfirmar.Name = "grpConfirmar";
            this.grpConfirmar.Size = new System.Drawing.Size(331, 90);
            this.grpConfirmar.TabIndex = 0;
            this.grpConfirmar.TabStop = false;
            this.grpConfirmar.Text = "Confirmar código";
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(250, 62);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 1;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Código:";
            // 
            // txtSmsCode
            // 
            this.txtSmsCode.Location = new System.Drawing.Point(95, 31);
            this.txtSmsCode.Name = "txtSmsCode";
            this.txtSmsCode.Size = new System.Drawing.Size(230, 20);
            this.txtSmsCode.TabIndex = 0;
            // 
            // Registro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 275);
            this.Controls.Add(this.grpConfirmar);
            this.Controls.Add(this.grpRequestCode);
            this.Name = "Registro";
            this.Text = " ";
            this.grpRequestCode.ResumeLayout(false);
            this.grpRequestCode.PerformLayout();
            this.grpConfirmar.ResumeLayout(false);
            this.grpConfirmar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRequestCode;
        private System.Windows.Forms.Button btnSolicitud;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.GroupBox grpConfirmar;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSmsCode;
    }
}

