﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhatsAppMessenger
{
    class General
    {
        private static AppData _bd;

        public static AppData BD
        {
            get
            {
                if (_bd == null)
                    _bd = new AppData();
                return _bd;
            }
        }
    }
}
