﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WhatsAppApi;

namespace WhatsAppMessenger
{
    public partial class InicioSesion : Form
    {
        WhatsApp wa;
        public InicioSesion()
        {
            InitializeComponent();
        }

        private void InicioSesion_Load(object sender, EventArgs e)
        {
            cerrarSesiónToolStripMenuItem.Visible = false;
            panel1.BringToFront();
            panel2.Enabled = false;
            listUsuarios.DisplayMember = "Display";
            listUsuarios.ValueMember = "Número de teléfono";
            if (Properties.Settings.Default.Recordar)
            {
                txtNumero.Text = Properties.Settings.Default.Telefono;
                txtPassword.Text = Properties.Settings.Default.Password;
                checkBox1.Checked = Properties.Settings.Default.Recordar;
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Recordar = checkBox1.Checked;
            Properties.Settings.Default.Telefono = txtNumero.Text;
            Properties.Settings.Default.Password = txtPassword.Text;
            Properties.Settings.Default.Save();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (Registro registro = new Registro())
            {
                if (registro.ShowDialog() == DialogResult.OK)
                {
                    txtNumero.Text = Properties.Settings.Default.Telefono;
                    txtPassword.Text = Properties.Settings.Default.Password;
                }
            }
        }

        private void informaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Informacion info = new Informacion())
            {
                info.ShowDialog();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (AñadirEditarUsuario edit = new AñadirEditarUsuario(listUsuarios.SelectedItem))
            {
                if (edit.ShowDialog() == DialogResult.OK)
                    LoadData();
                listUsuarios.SelectedItem = -1;
            }
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cerrarSesiónToolStripMenuItem.Visible = false;
            wa.Disconnect();
            panel2.Enabled = false;
            panel1.Enabled = true;
            panel1.BringToFront();
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            wa = new WhatsApp(Properties.Settings.Default.Telefono, Properties.Settings.Default.Password,
               Properties.Settings.Default.Nombre, true);
            wa.OnLoginSuccess += Wa_OnLoginSuccess;
            wa.OnLoginFailed += Wa_OnLoginFailed;
            wa.OnConnectFailed += Wa_OnConnectFailed;
        }

        private void Wa_OnLoginFailed(string data)
        {
            MessageBox.Show(string.Format("Inicio de sesión fallida: {0}", data), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);     
        }

        private void Wa_OnConnectFailed(Exception ex)
        {
            MessageBox.Show(string.Format("Conexion fallida: {0}", ex.StackTrace), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);            
        }

        private void Wa_OnLoginSuccess(string numeroTelefono, byte[] data)
        {
            panel1.Enabled = false;
            panel2.BringToFront();
            panel2.Enabled = true;
            cerrarSesiónToolStripMenuItem.Visible = true;
            General.BD.Usuario.Clear();
            General.BD.Cuenta.Clear();
            General.BD.AcceptChanges();
            string accountFile = string.Format("{0}\\ cuenta.dat", Application.StartupPath);
            if (File.Exists(accountFile))
                General.BD.Cuenta.ReadXml(accountFile);
            string userFile = string.Format("{0}\\usuario.dat", Application.StartupPath);
            if (File.Exists(userFile))
                General.BD.Usuario.ReadXml(userFile);
            LoadData();
        }

        private void LoadData()
        {
            var query = from o in General.BD.Usuario
                        where o.IdCuenta == Properties.Settings.Default.Telefono
                        select new
                        {
                            Telefono = o.IdUsuario,
                            Nombre = o.Nombre,
                            Display = string.Format("{0} (+{1})", o.Nombre, o.IdUsuario)
                        };
            listUsuarios.DataSource = query.ToList();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (listUsuarios.Items.Count > 0)
            {
                if (MessageBox.Show("¿Estás seguro de que desea eliminar este número?", "Mensaje",MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var obj = listUsuarios.SelectedItem;
                    if (obj != null)
                    {
                        AppData.UsuarioRow row = General.BD.Usuario.FindByIdUsuario(obj.GetType().GetProperty("Telefono").GetValue(obj, null).ToString());
                        General.BD.Usuario.RemoveUsuarioRow(row);
                        General.BD.Usuario.AcceptChanges();
                        General.BD.Usuario.WriteXml(string.Format("{0}\\usuario.dat", Application.StartupPath));
                        LoadData();
                        listUsuarios.SelectedItem = -1;
                    }
                }
            }
        }

        private void listUsuarios_DoubleClick(object sender, EventArgs e)
        {
            var obj = listUsuarios.SelectedItem;
            if (obj != null)
            {
                using (Chat chat = new Chat(obj, wa) { Text = obj.GetType().GetProperty("Nombre").GetValue(obj, null).ToString() })
                {
                    chat.ShowDialog();

                }
            }

        }
    }
}
