﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhatsAppMessenger
{
    public partial class Registro : Form
    {
        string password;
        public Registro()
        {
            InitializeComponent();
        }

        private void btnSolicitud_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("Por favor, ingrese su número de teléfono.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTelefono.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("Por favor, ingrese su nombre completo.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNombre.Focus();
                return;
            }
            if (WhatsAppApi.Register.WhatsRegisterV2.RequestCode(txtTelefono.Text, out password, "sms"))
            {
                if (!string.IsNullOrEmpty(password))
                    Save();
                else
                {
                    grpConfirmar.Enabled = true;
                    grpRequestCode.Enabled = false;
                }
            }
            else
                MessageBox.Show("No se ha podido generar la contraseña.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Save()
        {
            this.grpConfirmar.Enabled = false;
            this.grpRequestCode.Enabled = false;
            Properties.Settings.Default.Nombre = txtNombre.Text;
            Properties.Settings.Default.Telefono = txtTelefono.Text;
            Properties.Settings.Default.Password = password;
            Properties.Settings.Default.Save();
            if(General.BD.Cuenta.FindByIdCuenta(txtTelefono.Text) == null)
            {
                AppData.CuentaRow row = General.BD.Cuenta.NewCuentaRow();
                row.IdCuenta = txtTelefono.Text;
                row.Nombre = txtNombre.Text;
                row.Password = password;
                General.BD.Cuenta.AddCuentaRow(row);
                General.BD.Cuenta.AcceptChanges();
                General.BD.Cuenta.WriteXml(string.Format("{0}\\cuenta.dat", Application.StartupPath));
            }
            DialogResult = DialogResult.OK;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSmsCode.Text))
            {
                MessageBox.Show("Por favor, ingrese su código sms.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSmsCode.Focus();
                return;
            }
            password = WhatsAppApi.Register.WhatsRegisterV2.RegisterCode(txtTelefono.Text, txtSmsCode.Text);
            Save();
        }
    }
}
